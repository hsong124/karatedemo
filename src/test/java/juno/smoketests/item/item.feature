Feature: Smoke tests for the item endpoints

  Background:
    * url 'https://api.abcotvs.com/v3/kabc'

  Scenario: get all users and then get the first user by id

    Given path 'item', 'kabc', 'meta', 'tvlistings'
    And param key = "test"
    When method get
    Then status 200