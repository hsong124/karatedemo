## Description

This repository should be of help for the team to start getting familiar with the Karate framework.

## Project structure (and where to place the files)

All tests, with no exception, must be placed under the `src/test/main/java` folder. Inside this folder, the tests will
be organized in packages by the project under test, the test type (smoke, end-to-end, and so on), and lastly, by the
endpoint that's been tested.

## Running the tests

### From Intellij Idea

Right-click on the Scenario/Feature and then click on `Run 'Scenario/Feature: ...`.

You can also run any runner in the project by clicking on the green arrow that appears next to the runner's class.

### From the command line

You can run `mvn test -Dtest=JunoRunner` in your terminal.

Alternatively, you can run `mvn test -Dtest=juno.smoketests.SmokeTestsRunner` in your terminal.

## IDE support and configuration

Please, consult [Karate's documentation on this matter](https://github.com/intuit/karate/wiki/IDE-Support).

## Testing data

You can use `https://api.abcotvs.com/v3/kabc` as the API under test, and whichever endpoint from the following list:

### Archive CMS 7.0
* `/v3/archive/5744098.json`

### Config endpoint
* `/v3/kabc/config/ads`

### Juno endpoints
* `/v3/kabc/item/7650`
* `/v3/kabc/item/kabc/meta/tvlistings`
* `/v3/kgo/list`
* `/v3/kabc/list?q=test%20snippet`
* `/v3/kgo/list?tag=sports`
* `/v3/kgo/list?topic=weather`
* `/v3/kgo/list?place=california`
* `/v3/kgo/list?property=fbia_gptext&values=true`
* `/v3/kabc/list/4667224.related`
* `/v3/kabc/list/kgo-spotlight/spotlightContent`
* `/v3/kabc/list/ccg/video`
* `/v3/kabc/list/ccg/featured`
* `/v3/kabc/list/ccg/event`
* `/v3/kabc/list/master.all`
* `/v3/kabc/list/master.featured`
* `/v3/kabc/list/master.news`
* `/v3/kabc/list/master.photogalleries`
* `/v3/kabc/list/master.promotions`
* `/v3/kabc/list/master.videos`
* `/v3/kabc/list?q=test%20snippet`
* `/v3/ktrk/list/photos`
* `/v3/kgo/list/post`
* `/v3/kgo/list/post?dcsData=true`
* `/v3/kabc/list/videos`
* `/v3/kabc/list/firetv.feelgood`
* `/v3/kabc/list/videos.firetv?limit=28&topic=weather&fromDate=2019-1-22`
* `/v3/kabc/list/videos.firetv?topicExclude=weather&limit=28`
* `/v3/kabc/list/videos.firetv?limit=28&topic=weather&fromDate=2019-1-18`

### School closings
* `/v3/wabc/school-closings/closings.json`

### Weather svc endpoints
* `/v3/wabc/weather`
* `/v3/wabc/weather/cityid/349727`
* `/v3/wabc/weather/zipcode/98014`
* `/v3/wabc/weather/forecast`
* `/v3/wabc/weather/forecast/cityid/351409`
* `/v3/wabc/weather/forecast/zipcode/98014`
* `/v3/wabc/weather/locations/city/Bellevue`
* `/v3/wabc/weather/locations/state/wa`
* `/v3/wabc/weather/locations/state/wa/city/Bellevue`
* `/v3/wabc/weather/locations/zipcode/12345`
* `/v3/wabc/weather/locations/search?q=seat`

### News schedule
* `/v3/kabc/news/kabc_schedule.xml`
* `/v3/kfsn/news/kfsn_schedule.xml`
* `/v3/kgo/news/kgo_schedule.xml`
* `/v3/ktrk/news/ktrk_schedule.xml`
* `/v3/wabc/news/wabc_schedule.xml`
* `/v3/wpvi/news/wpvi_schedule.xml`
* `/v3/wls/news/wls_schedule.xml`
* `/v3/wtvd/news/wtvd_schedule.xm`